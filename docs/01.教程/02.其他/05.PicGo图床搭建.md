---
title: PicGo图床搭建
date: 2021-12-18 21:34:47
tags: null
categories: 
 - 教程
 - 其他 
permalink: /pages/2a9112/
author:
 name: vivid
   
---
<!--more-->
## 下载PicGo

```shell
https://github.com/Molunerfinn/PicGo/releases
```
## 安装插件

```
gitee-uploader
```



## 配置

```
6080c0e8c929b628803f969299250b8856c26c9e
MyhangsYourdream/cloud-img
img/
https://cdn.jsdelivr.net/gh/MyhangsYourdream/cloud-img/
#码云配置

70a62534fa7b66c2e9af26e27986d636
vividcode/pic-go
master

```

![image-20211221204222977](https://gitee.com/vividcode/pic-go/raw/master/2021/202112212124433.png)

![image-20211221212700485](https://gitee.com/vividcode/pic-go/raw/master/2021/202112212127207.png)